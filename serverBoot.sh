#!/bin/bash

##
## Startup script for Turbine - Hystrix Aggregation Server.  This will set the needed environment variables and
## boot up the application using the spring boot fat jar that is the artifact of the
## production build.
##

CONFIG_APPLICATION=mi-cloud-turbine
CONFIG_APPLICATION_EXTENSION=jar
CONFIG_VERSION=0.0.1-SNAPSHOT
PATH_TO_EXECUTABLE=target # <-- no trailing slash here.
# jvm arguments added here
JVM_OPTS="" #"-Dspring.profiles.active=production"

export SERVER_PORT=8990
export AMQP_PORT=8989
export EUREKA_SERVER_URI=http://127.0.0.1:8761/micloudeureka
export HOSTNAME=127.0.0.1


java ${JVM_OPTS} -jar ${PATH_TO_EXECUTABLE}/${CONFIG_APPLICATION}-${CONFIG_VERSION}.${CONFIG_APPLICATION_EXTENSION}
