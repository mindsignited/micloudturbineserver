package com.mindsignited;

import com.mindsignited.keycloak.resource.EnableKeycloakOAuth2Resource;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

/**
 * @author Spencer Gibb
 * @author Dave Syer
 */
@SpringBootApplication
//@EnableTurbineAmqp
@EnableTurbine
@EnableDiscoveryClient
@EnableKeycloakOAuth2Resource
public class TurbineApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(TurbineApplication.class).run(args);
    }
}
