#!/bin/bash

## MI Cloud Turbine Server

export SERVER_PORT=8990
export AMQP_PORT=8989
export EUREKA_SERVER_URI=http://localhost:8761/micloudeureka

if [ "$1" == "debug" ]; then
    mvn spring-boot:run -Drun.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5010" --debug
else
    mvn spring-boot:run $@
fi
